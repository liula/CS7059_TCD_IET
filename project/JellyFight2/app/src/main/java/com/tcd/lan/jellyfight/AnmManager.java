package com.tcd.lan.jellyfight;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/18.
 */

public class AnmManager {
    Game game;
    ArrayList<JFSpriteAnmInterface>anmList;
    public AnmManager(Game game){
        this.game = game;
        anmList= new ArrayList<JFSpriteAnmInterface>();

    }

    public boolean handle(){
        ArrayList<JFSpriteAnmInterface> needRemove= new ArrayList<JFSpriteAnmInterface>();
        //System.out.println(anmList.size());
        for(JFSpriteAnmInterface anm:anmList){
            anm.process();

            if(anm.removeSelf()){

                needRemove.add(anm);;
            }
        }

        for(JFSpriteAnmInterface anm:needRemove){
            anmList.remove(anm);
        }
        if(anmList.size()>0){
            return true;

        }else{
            return false;
        }
    }

    public void add(JFSpriteAnmInterface anm){

        anmList.add(anm);
    }

    public void remove(JFSpriteAnmInterface anm){
        anmList.remove(anm);
    }
}
