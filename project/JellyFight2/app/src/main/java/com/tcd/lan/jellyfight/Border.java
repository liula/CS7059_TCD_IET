package com.tcd.lan.jellyfight;

import com.tcd.lan.jellyfight.player.JellyFishPlayer;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/12.
 */

public class Border {

    ArrayList<Borderline> border ;

    public Border(){
        border =new ArrayList<Borderline>();
    }


    public void add(Borderline line){
        //line.setColor(1,0,0);
        line.setAlpha(0);
        border.add(line);
    }

    public int getLineNum(){
        return border.size();
    }
    public Borderline getLine(int i){
        return border.get(i);
    }


    public boolean checkCollision(JellyFishPlayer player){
        for(Borderline b:border){
            if(b.collidesWith(player)){
                return true;
            }
        }
        return false;
    }





}
