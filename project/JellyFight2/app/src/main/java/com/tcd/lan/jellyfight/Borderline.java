package com.tcd.lan.jellyfight;

import org.andengine.entity.primitive.Line;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/12.
 */

public class Borderline extends Line {


    public Borderline(float pX1, float pY1, float pX2, float pY2, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX1, pY1, pX2, pY2, pVertexBufferObjectManager);
    }


}
