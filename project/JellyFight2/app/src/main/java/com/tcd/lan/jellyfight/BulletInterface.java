package com.tcd.lan.jellyfight;

import com.tcd.lan.jellyfight.player.JFTroop;

import org.andengine.entity.shape.IShape;

/**
 * Created by vlery on 2016/11/15.
 */

public interface BulletInterface  extends IShape{
    public void process();
    public boolean checkActiveRequest();
}
