package com.tcd.lan.jellyfight;

import com.tcd.lan.jellyfight.item.Coin;
import com.tcd.lan.jellyfight.item.CoinManager;
import com.tcd.lan.jellyfight.item.NewPlayerItem;
import com.tcd.lan.jellyfight.monster.FrogBullet;
import com.tcd.lan.jellyfight.monster.FrogMonster;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.player.JFTroop;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.player.PlayerBulletInterface;
import com.tcd.lan.jellyfight.player.PlayerType;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;
import com.tcd.lan.jellyfight.util.Tool;

import org.andengine.entity.scene.Scene;

import java.util.Random;

/**
 * Created by vlery on 2016/11/12.
 */

public class Game {
    boolean ifEnd= false;

    MainActivity activity;
    Scene scene;

    int score;
    static int PASS_SCORE =60;
    JFTroop troop;
    JellyFishPlayer leader;

    CoinManager coinManager;

    MonsterManager monsterManager;

    AnmManager anmManager ;

    boolean ifWin;

    float BORDER_WIDTH = 80;
    float x_min,x_max,y_min,y_max;
    //Border
    Border border;


    public Game(MainActivity activity,Scene scene){

        coinManager = new CoinManager(this);
        score= 0;
        troop=new JFTroop(this);
        this.activity= activity;
        this.scene = scene;
        border = new Border();
        initBorder();
        monsterManager= new MonsterManager(this);
        anmManager = new AnmManager(this);
        ifWin=false;

    }

    public void initBorder(){

        setBorder1();
        for(int i=0;i<border.getLineNum();i++){
            scene.attachChild(border.getLine(i));
        }
    }
    public void setBounds(float x,float y){
        this.x_min = BORDER_WIDTH;
        this.x_max = x-BORDER_WIDTH;
        this.y_min = BORDER_WIDTH;
        this.y_max = y-BORDER_WIDTH;

    }

    public void initTroop(float ini_x,float ini_y,Direction ini_dir){

        leader = activity.generatePlayer(troop.getRandomUnusedType(),ini_x,ini_y);
        leader.initBloodBar(activity.generateBloodBar(leader));
        scene.attachChild(leader);
        scene.attachChild(leader.getBloodBar());
        leader.setDirection(ini_dir);
        troop.addMember(leader);

    }
    public JellyFishPlayer getLeader(){
        return leader;
    }

    public void process(){
        troop.step();
        if (border.checkCollision(troop.getLeader())) {
            end();
        }

        if(monsterManager.checkCollision(troop.getLeader())){
            end();
        }

        coinManager.handle();
        monsterManager.handle();
        anmManager.handle();
        if(score>PASS_SCORE){
            ifWin = true;
            end();
        }
    }

    public boolean finishFollowUp(){
        boolean ifFinish = true;
        monsterManager.finishBullet();

       if(! anmManager.handle()){
           ifFinish= false;
       }
        troop.finishBullet();
        return ifFinish;
    }
    public void end(){
        ifEnd= true;
        if(!ifWin) {
            troop.killTroop(troop);
        }

    }


    public boolean ifWin(){
        return ifWin;
    }

    public boolean ifEnd(){
        return ifEnd;
    }







    public void addCoin(int type){
        Point p= calculateVacantLoc();
        Coin coin = activity.generateCoin(p.getX(),p.getY());
        coin.setCurrentTileIndex(type);
        scene.attachChild(coin);
        scene.sortChildren();
        coinManager.addCoin(coin);
    }
    public void addCoin(float x,float y,int type){
        Coin coin = activity.generateCoin(x,y);
        coin.setCurrentTileIndex(type);
        scene.attachChild(coin);
        scene.sortChildren();
        coinManager.addCoin(coin);
    }
    public void addScore(int i){
        score+=i;
    }

    public int getScore(){
        return score;
    }
    public void addPlayer(){
       // JellyFishPlayer player =activity.generatePlayer(troop.getRandomUnusedType(),0,0);
        JellyFishPlayer player =activity.generatePlayer(troop.getRandomUnusedType(),0,0);

        troop.addMember( player );
        player.initBloodBar(activity.generateBloodBar(player));
        scene.attachChild(player);
        scene.attachChild(player.getBloodBar());
        scene.sortChildren();
    }


    public void addPlayer(PlayerType type){
        // JellyFishPlayer player =activity.generatePlayer(troop.getRandomUnusedType(),0,0);
        JellyFishPlayer player =activity.generatePlayer(type,0,0);

        troop.addMember( player );
        player.initBloodBar(activity.generateBloodBar(player));
        scene.attachChild(player);
        scene.attachChild(player.getBloodBar());
        scene.sortChildren();
    }
    private void setBorder1(){
        border.add(activity.generateBorderLine(40,40,600,40));

        border.add(activity.generateBorderLine(40,40,40,600));
        border.add(activity.generateBorderLine(40,600,600,600));
        border.add(activity.generateBorderLine(600,40,600,600));


    }

    private void setBorder(){
        int [][] upper_polyline={{0,0},{8,6},{16,-3},{28,1},{32,-1}};
        int [][] right_polyline ={{2,0},{-3,6},{3,12},{-3,21},{2,32}};
        int [][]down_polyline ={{0,1},{-4,3},{-10,-1},{-18,3},{-25,0},{-32,1}};
        int [][] left_polyline ={{-2,0},{3,-9},{-1,-19},{3,-26},{-2,-32}};
        border.add(activity.generateBorderLine(48,64,64,48));
        for(int i=0;i<16;i++){
            int x=64+i*32;
            int y=48;
            for(int j=1;j<upper_polyline.length;j++)
                border.add(activity.generateBorderLine(x+upper_polyline[j-1][0],y+upper_polyline[j-1][1]-5,
                        x+upper_polyline[j][0],y+upper_polyline[j][1]-5));
        }
        border.add(activity.generateBorderLine(576,48,592,64));
        for(int i=0;i<16;i++){
            int x=592;
            int y=64+32*i;
            for(int j=1;j<right_polyline.length;j++)
                border.add(activity.generateBorderLine(x+right_polyline[j-1][0]+5,y+right_polyline[j-1][1],
                        x+right_polyline[j][0]+5,y+right_polyline[j][1]));
        }
        border.add(activity.generateBorderLine(592,576,576,592));
        for(int i=0;i<16;i++){
            int x=576-i*32;
            int y=592;
            for(int j=1;j<down_polyline.length;j++)
                border.add(activity.generateBorderLine(x+down_polyline[j-1][0],y+down_polyline[j-1][1]+5,
                        x+down_polyline[j][0],y+down_polyline[j][1]+5));
        }
        border.add(activity.generateBorderLine(64,592,48,576));
        for(int i=0;i<16;i++){
            int x=48;
            int y=576-i*32;
            for(int j=1;j<left_polyline.length;j++)
                border.add(activity.generateBorderLine(x+left_polyline[j-1][0]-5,y+left_polyline[j-1][1],
                        x+left_polyline[j][0]-5,y+left_polyline[j][1]));
        }

    }

    public void turn(Direction direction){
        switch(direction){
            case RIGHT:
                if(troop.getLeaderDirection().equals(Direction.LEFT)||troop.getLeaderDirection().equals(Direction.RIGHT)){
                }else {
                    troop.turn(Direction.RIGHT);
                }
                break;
            case LEFT:
                if(troop.getLeaderDirection().equals(Direction.LEFT)||troop.getLeaderDirection().equals(Direction.RIGHT)){
                }else {
                    troop.turn(Direction.LEFT);
                }
                break;
            case UP:
                if(troop.getLeaderDirection().equals(Direction.DOWN)||troop.getLeaderDirection().equals(Direction.UP)){
                }else {
                    troop.turn(Direction.UP);
                }
                break;
            case DOWN:
                if(troop.getLeaderDirection().equals(Direction.DOWN)||troop.getLeaderDirection().equals(Direction.UP)){
                }else {
                    troop.turn(Direction.DOWN);
                }
                break;




        }
    }

    public Point calculateVacantLoc(){
        float pos_x,pos_y;
        boolean unique= false;
        Random random = new Random();


        do{

            pos_x = random.nextFloat()*(x_max-x_min)+x_min;
            pos_y = random.nextFloat()*(y_max-y_min)+y_min;

            if(!coinManager.checkLocVacancy(pos_x,pos_y)){
                unique = false;break;
            }
            unique = troop.checkLocVacancy(pos_x,pos_y);
        }while(!unique);


        return new Point(pos_x,pos_y);
    }

    public void addFrogMonster(){
        Point p= calculateVacantLoc();
        FrogMonster frog= activity.generateFrogMonster(p.getX(),p.getY());
        frog.initBloodBar(activity.generateMonsterBloodBar(frog));
        scene.attachChild(frog);
        scene.attachChild(frog.getBloodBar());
        scene.sortChildren();
        monsterManager.addMonster(frog);
    }

    public JFTroop getTroop(){
        return troop;
    }



    public void addMonsterBullet(ATTACK type,Point start,Point target){

        Point v= new Point(target.getX()-start.getX(),target.getY()-start.getY()).normalize();

        switch (type) {
            case FROMFROG:
                    FrogBullet bullet= (FrogBullet) activity.generateMonsterBullet(type, start);
                    bullet.setCurrentTileIndex(1);
                    //System.out.println(-Tool.getRotate(start,target));
                    bullet.setRotation(-Tool.getRotate(start,target));

                    bullet.setVolecity((float)(v.getX()*2.3),(float)(v.getY()*2.3));
                    scene.attachChild(bullet);
                    monsterManager.addBullet(bullet);
                    scene.sortChildren();
                    break;
        }
    }

    public void addPlayerBullet(ATTACK type,Point start,Point target){
        Point v= new Point(target.getX()-start.getX(),target.getY()-start.getY()).normalize();
       // System.out.println("start:::"+ start.getX()+"::::"+start.getY());
        //System.out.println("v:::"+ v.getX()+"::::"+v.getY());
        PlayerBulletInterface  bullet = activity.generatePlayerBullet(type,start);
        bullet.setVolecity((float)(v.getX()*2.5),(float)(v.getY()*2.5));
        scene.attachChild(bullet);
        troop.addBullet(bullet);
        scene.sortChildren();


    }
    public Border getBorder(){
        return border;
    }

    public void addLightEffect(LightEffect type,float x,float y){
        LightEffectAnm effect = activity.generateLightEffectAnm(x,y);
        switch(type){
            case GETCOIN:
                effect.setIndex(0);
                break;
            case FROGBULLETATTACK:
                effect.setIndex(4);
                break;
            case PLAYER_A_ATTACK:
            case PLAYER_B_ATTACK:
            case PLAYER_C_ATTACK:
            case PLAYER_D_ATTACK:
                effect.setIndex(8);
                break;

        }
        scene.attachChild(effect);
        scene.sortChildren();
        anmManager.add(effect);

    }

    public MonsterManager getMonsterManager(){
        return monsterManager;
    }


    public void addMonsterDieAnm(float x,float y){

        JFDiePlayerAnm anm=activity.generateDieAnm(x- 12,y-12);
        scene.attachChild(anm);
        scene.sortChildren();
        anmManager.add(anm);
    }

    public void addMonsterDeathReward(float x,float y){
        double random = Math.random();
        if(random<0.2){
       // if(random<1){
            if(!troop.ifAllTypeUse()) {
                NewPlayerItem player = activity.generateNewPlayerItem(troop.getRandomUnusedType(),new Point(x-Config.PLAYER_X_OFFSET,y-Config.PLAYER_Y_OFFSET));
                troop.addNewPlayerItem(player);
                scene.attachChild(player);
                scene.sortChildren();
            }
        }else if (random<0.7){
            Coin coin =activity.generateCoin(x-Config.COIN_POS_X_OFFSET,y-Config.COIN_POS_Y_OFFSET);
            coinManager.addCoin(coin);
            scene.attachChild(coin);
            scene.sortChildren();
        }
    }

    public void addPlayerDeathAnm(float x,float y){

        JFDiePlayerAnm anm=activity.generateDieAnm(x-Config.LE_ANM_WIDTH_OFFSET,y-Config.LE_ANM_HEIGHT_OFFSET);
        scene.attachChild(anm);
        scene.sortChildren();
        anmManager.add(anm);
    }

    public JellyFishPlayer getNewPhamton(){
        return activity.generatePlayer(PlayerType.A,0,0);
    }
}
