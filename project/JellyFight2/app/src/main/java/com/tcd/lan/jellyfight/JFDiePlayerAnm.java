package com.tcd.lan.jellyfight;

import com.tcd.lan.jellyfight.AnmManager;
import com.tcd.lan.jellyfight.JFSpriteAnmInterface;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/13.
 */

public class JFDiePlayerAnm extends TiledSprite implements JFSpriteAnmInterface {
    boolean ifEnd;

    int count;
    static int MAX_COUNT= 3;
    int tileIndex;
    static int MAX_TILED =6;
    public JFDiePlayerAnm(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {

        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        ifEnd= false;

        tileIndex =0;
        setCurrentTileIndex(tileIndex);
        count =0;
    }




    @Override
    public boolean removeSelf() {

        return ifEnd;
    }

    @Override
    public void process() {
        if(!ifEnd){
            count++;
            count%=MAX_COUNT;
            if(count==0){
                tileIndex++;

                if(tileIndex<=MAX_TILED){
                    setCurrentTileIndex(tileIndex);

                }else{

                    ifEnd=true;
                    detachSelf();

                }
            }
        }
    }
}
