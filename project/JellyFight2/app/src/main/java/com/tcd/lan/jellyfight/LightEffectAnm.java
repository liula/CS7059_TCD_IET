package com.tcd.lan.jellyfight;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/18.
 */

public class LightEffectAnm extends TiledSprite implements  JFSpriteAnmInterface {

    int count ;
    static int  MAX_COUNT=5;

    int index;
    boolean ifEnd;
    static int Z_INDEX =1;

    int tileIndex,MAX_TILED;

    int i=0;
    public LightEffectAnm(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setZIndex(Z_INDEX);
        ifEnd= false;
        count=0;
    }

    public void setIndex(int index){
        this.index= index;
        tileIndex=index;
        MAX_TILED= index+3;
        this.setCurrentTileIndex(tileIndex);
    }




    @Override
    public boolean removeSelf() {

        return ifEnd;
    }

    @Override
    public void process() {

        if(!ifEnd){

            count++;
            count%=MAX_COUNT;

            if(count==0){
                tileIndex++;
                if(tileIndex<=MAX_TILED){
                    setCurrentTileIndex(tileIndex);
                }else{
                    ifEnd=true;
                    detachSelf();
                }
            }
        }
    }
}
