package com.tcd.lan.jellyfight;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.debug.Debug;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLES20;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.tcd.lan.jellyfight.item.Coin;
import com.tcd.lan.jellyfight.item.NewPlayerItem;
import com.tcd.lan.jellyfight.monster.FrogBullet;
import com.tcd.lan.jellyfight.monster.FrogMonster;
import com.tcd.lan.jellyfight.monster.Monster;
import com.tcd.lan.jellyfight.monster.MonsterBloodBar;
import com.tcd.lan.jellyfight.monster.MonsterBulletInterface;
import com.tcd.lan.jellyfight.player.JFPlayerBloodBar;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.player.JellyFishPlayerA;
import com.tcd.lan.jellyfight.player.JellyFishPlayerB;
import com.tcd.lan.jellyfight.player.JellyFishPlayerC;
import com.tcd.lan.jellyfight.player.JellyFishPlayerD;
import com.tcd.lan.jellyfight.player.PlayerABullet;
import com.tcd.lan.jellyfight.player.PlayerBBullet;
import com.tcd.lan.jellyfight.player.PlayerBulletInterface;
import com.tcd.lan.jellyfight.player.PlayerCBullet;
import com.tcd.lan.jellyfight.player.PlayerDBullet;
import com.tcd.lan.jellyfight.player.PlayerType;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;

public class MainActivity extends SimpleBaseGameActivity  implements IOnSceneTouchListener {
    //FrogBullet bullet;
    // ===========================================================
    // Constants
    // ===========================================================


  //  private static final float TROOP_SPEED = 60;
    //screen info
    int deviceWidth, deviceHeight;

    //
    int bounds_x, bounds_y;
    //param for touch sweep
    double[] p_down = new double[2];
    double[] p_up = new double[2];
    // ===========================================================
    // Fields
    // ===========================================================

    private BoundCamera mBoundChaseCamera;

    private BitmapTextureAtlas mBitmapPlayerATextureAtlas;
    private TiledTextureRegion mPlayerATextureRegion;
    private BitmapTextureAtlas mBitmapPlayerBTextureAtlas;
    private TiledTextureRegion mPlayerBTextureRegion;
    private BitmapTextureAtlas mBitmapPlayerCTextureAtlas;
    private TiledTextureRegion mPlayerCTextureRegion;
    private BitmapTextureAtlas mBitmapPlayerDTextureAtlas;
    private TiledTextureRegion mPlayerDTextureRegion;

    private BitmapTextureAtlas mPlayerAttackBallAtlas;
    private TiledTextureRegion mPlayerAttackBallRegion;

    private BitmapTextureAtlas mSmokeTextureAtlas;
    private TiledTextureRegion mPlayerDieRegion;

    private BitmapTextureAtlas mBarTextureAtlas;
    private TiledTextureRegion mBarTextureRegion;

    private BitmapTextureAtlas mBar1TextureAtlas;
    private TiledTextureRegion mBar1TextureRegion;

    private BitmapTextureAtlas mstFrogTextureAtlas;
    private TiledTextureRegion mstFrogTextureRegion;

    private BitmapTextureAtlas mCoinTextureAtlas;
    private TiledTextureRegion mCoinTextureRegion;


    private BitmapTextureAtlas mstAttackTextureAtlas;
    private TiledTextureRegion mstAttackTextureRegion;

    private BitmapTextureAtlas mLightEffectTextureAtlas;
    private TiledTextureRegion mLightEffectTextureRegion;

    private TMXTiledMap mTMXTiledMap;
    private Font font;

    private Text mScoreText;
    //Troop list
  //  private int troop_size = 4;


    //Game
    Game game;

    Scene scene;
    private HUD hud;

    //bool
    boolean ifPlayerBarLoad, ifMonsterPlayerBarLoad, ifLightEffectLoad, ifGoldCoinLoad, ifSmokeLoad, ifMonster1Load, ifMonsterBulletLoad = false;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public EngineOptions onCreateEngineOptions() {
        //Toast.makeText(this, "The tile the player is walking on will be highlighted.", Toast.LENGTH_LONG).show();
        // new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT),new FillResolutionPolicy()
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        deviceWidth = displayMetrics.widthPixels / 4;
        deviceHeight = displayMetrics.heightPixels / 4;
        this.mBoundChaseCamera = new BoundCamera(0, 0, deviceWidth, deviceHeight);
        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(deviceWidth, deviceHeight), this.mBoundChaseCamera);
        // this.mBoundChaseCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

        //return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mBoundChaseCamera);
    }

    @Override
    public void onCreateResources() {
        FontFactory.setAssetBasePath("font/");
        font = FontFactory.createFromAsset(this.getFontManager(), this.getTextureManager(), 512, 512, TextureOptions.BILINEAR, this.getAssets(), "Plok.ttf", 16, true, android.graphics.Color.BLACK);
        font.load();

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        this.mBitmapPlayerATextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerATextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerATextureAtlas, this, "JFplayer1.png", 0, 0, 3, 4);
        this.mBitmapPlayerATextureAtlas.load();

        this.mBitmapPlayerBTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerBTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerBTextureAtlas, this, "JFplayer2.png", 0, 0, 3, 4);
        this.mBitmapPlayerBTextureAtlas.load();

        this.mBitmapPlayerCTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerCTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerCTextureAtlas, this, "JFplayer3.png", 0, 0, 3, 4);
        this.mBitmapPlayerCTextureAtlas.load();

        this.mBitmapPlayerDTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerDTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerDTextureAtlas, this, "JFplayer4.png", 0, 0, 3, 4);
        this.mBitmapPlayerDTextureAtlas.load();

        this.mPlayerAttackBallAtlas = new BitmapTextureAtlas(this.getTextureManager(), 40, 10, TextureOptions.DEFAULT);
        this.mPlayerAttackBallRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mPlayerAttackBallAtlas, this, "attackBall.png", 0, 0, 4, 1);
        this.mPlayerAttackBallAtlas.load();

        this.mSmokeTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 96, TextureOptions.DEFAULT);
        this.mPlayerDieRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mSmokeTextureAtlas, this, "smoke.png", 0, 0, 3, 3);

        this.mBarTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 60, TextureOptions.DEFAULT);
        this.mBarTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBarTextureAtlas, this, "bar.png", 0, 0, 3, 4);

        this.mBar1TextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 60, TextureOptions.DEFAULT);
        this.mBar1TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBar1TextureAtlas, this, "bar1.png", 0, 0, 3, 4);

        this.mCoinTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 10, 13, TextureOptions.DEFAULT);
        this.mCoinTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mCoinTextureAtlas, this, "goldCoin.png", 0, 0, 1, 1);

        //mst load
        this.mstFrogTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 96, 18, TextureOptions.DEFAULT);
        this.mstFrogTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mstFrogTextureAtlas, this, "monster1.png", 0, 0, 4, 1);

        this.mstAttackTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 40, 10, TextureOptions.DEFAULT);
        this.mstAttackTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mstAttackTextureAtlas, this, "attack.png", 0, 0, 4, 1);

        this.mLightEffectTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 200, 300, TextureOptions.DEFAULT);
        this.mLightEffectTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mLightEffectTextureAtlas, this, "light_effect.png", 0, 0, 4, 6);

        this.mBarTextureAtlas.load();
        this.mBar1TextureAtlas.load();
    }

    @Override
    public Scene onCreateScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());
        hud = new HUD();
        this.mBoundChaseCamera.setHUD(hud);



         mScoreText = new Text(10, 10,font , "Score : 0", "Score : XXXX".length(), this.getVertexBufferObjectManager());
       // mScoreText.setPosition((deviceWidth - mScoreText.getWidth()) , 10f);
        mScoreText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        mScoreText.setAlpha(1.0f);

      //  super_scene.attachChild(mScoreText);
        scene = new Scene();



        game = new Game(this, scene);




        //super_scene.attachChild(mScoreText);

        hud.attachChild(mScoreText);
        //change troop direction
        scene.setOnSceneTouchListener(this);

        //load map
        try {
            final TMXLoader tmxLoader = new TMXLoader(this.getAssets(), this.mEngine.getTextureManager(), TextureOptions.BILINEAR_PREMULTIPLYALPHA, this.getVertexBufferObjectManager(), new ITMXTilePropertiesListener() {
                @Override
                public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {

                }
            });
            this.mTMXTiledMap = tmxLoader.loadFromAsset("tmx/seabed.tmx");
        } catch (final TMXLoadException e) {
            Debug.e(e);
        }

        final TMXLayer tmxLayer = this.mTMXTiledMap.getTMXLayers().get(0);
        bounds_x = tmxLayer.getWidth();
        bounds_y = tmxLayer.getHeight();

        final TMXLayer tmexLayer_border = this.mTMXTiledMap.getTMXLayers().get(1);
        scene.attachChild(tmxLayer);
        scene.attachChild(tmexLayer_border);

		/* Make the camera not exceed the bounds of the TMXEntity. */
        this.mBoundChaseCamera.setBounds(0, 0, tmxLayer.getHeight(), tmxLayer.getWidth());
        this.mBoundChaseCamera.setBoundsEnabled(true);

        /*set border*/
        game.setBounds(bounds_x, bounds_y);

        /* Calculate the coordinates for the face, so its centered on the camera. */
        int centerX = bounds_x / 2;
        int centerY = bounds_y / 2;
        game.initTroop(centerX, centerY, Direction.DOWN);
        this.mBoundChaseCamera.setChaseEntity(game.getLeader());

//        for (int i = 1; i < troop_size; i++) {
//            game.addPlayer();
//        }


		/* Now we are going to create a rectangle that will  always highlight the tile below the feet of the pEntity. */
//        final Rectangle currentTileRectangle = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight(), this.getVertexBufferObjectManager());
//        currentTileRectangle.setColor(1, 0, 0, 0.25f);
//        scene.attachChild(currentTileRectangle);
        scene.registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void reset() {
            }

            @Override
            public void onUpdate(final float pSecondsElapsed) {
                if (!game.ifEnd()) {
                    mScoreText.setText("Score : " + game.getScore());
                    game.process();
                } else {
                    if(game.finishFollowUp()){
                        if(game.ifWin()) {
                           jumpTowin();
                        }else{
                            jumpToLose();
                        }
                    }

                }


            }
        });

        return scene;
    }

    public void jumpTowin(){
        this.startActivity(new Intent(this, WinActivity.class));
        finish();
    }
    public void jumpToLose(){
        this.startActivity(new Intent(this, LoseActivity.class));
        finish();
    }
    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

        switch (pSceneTouchEvent.getAction()) {
            case TouchEvent.ACTION_DOWN:
                Toast.makeText(MainActivity.this, "down", Toast.LENGTH_SHORT).show();
                p_down[0] = pSceneTouchEvent.getX();
                p_down[1] = pSceneTouchEvent.getY();
                break;
            case TouchEvent.ACTION_UP:
                Toast.makeText(MainActivity.this, "up", Toast.LENGTH_SHORT).show();
                p_up[0] = pSceneTouchEvent.getX();
                p_up[1] = pSceneTouchEvent.getY();
                double moveX = p_up[0] - p_down[0];
                double moveY = p_up[1] - p_down[1];

                if (Math.abs(moveX) > 1.2 * Math.abs(moveY)) {
                    if (moveX > 0) {
                        game.turn(Direction.RIGHT);

                    } else {
                        game.turn(Direction.LEFT);

                    }
                } else if (Math.abs(moveY) > 1.2 * Math.abs(moveX)) {
                    if (moveY > 0) {
                        game.turn(Direction.DOWN);
                    } else {
                        game.turn(Direction.UP);

                    }
                }
                break;
        }
        return false;
    }


    public JFDiePlayerAnm generateDieAnm(float x, float y) {
        if (!ifSmokeLoad) {
            this.mSmokeTextureAtlas.load();
            ifSmokeLoad = true;
        }
        return new JFDiePlayerAnm(x, y, this.mPlayerDieRegion, this.getVertexBufferObjectManager());
    }

    public Coin generateCoin(float x, float y) {
        if (!ifGoldCoinLoad) {
            this.mCoinTextureAtlas.load();
            ifGoldCoinLoad = true;
        }
        return new Coin(x, y, this.mCoinTextureRegion, this.getVertexBufferObjectManager());
    }


    public JellyFishPlayer generatePlayer(PlayerType type, float x, float y) {
        JellyFishPlayer player;
        switch (type) {
            case A:
                player= new JellyFishPlayerA(x, y, this.mPlayerATextureRegion, this.getVertexBufferObjectManager());
                break;
            case B:
                player= new JellyFishPlayerB(x, y, this.mPlayerBTextureRegion, this.getVertexBufferObjectManager());
                break;
            case C:
                player =new JellyFishPlayerC(x, y, this.mPlayerCTextureRegion, this.getVertexBufferObjectManager());
                break;
            case D:
                player= new JellyFishPlayerD(x, y, this.mPlayerDTextureRegion, this.getVertexBufferObjectManager());
                break;
            default: player= new JellyFishPlayerA(x, y, this.mPlayerATextureRegion, this.getVertexBufferObjectManager());
        }
        player.setType(type);
        return player;
    }


    public JFPlayerBloodBar generateBloodBar(JellyFishPlayer player) {


        return new JFPlayerBloodBar(player.getX(), player.getY() + player.getBarYOffset(), mBarTextureRegion, this.getVertexBufferObjectManager());

    }

    public MonsterBloodBar generateMonsterBloodBar(Monster monster) {

        return new MonsterBloodBar(monster.getX(), monster.getY() + monster.getBarYOffset(), mBar1TextureRegion, this.getVertexBufferObjectManager());

    }

    public Borderline generateBorderLine(float x1, float y1, float x2, float y2) {
        return new Borderline(x1, y1, x2, y2, this.getVertexBufferObjectManager());
    }


    public FrogMonster generateFrogMonster(float x, float y) {
        if (!ifMonster1Load) {
            this.mstFrogTextureAtlas.load();
            ifMonster1Load = true;
        }
        FrogMonster frog = new FrogMonster(x, y, this.mstFrogTextureRegion, this.getVertexBufferObjectManager());
        frog.setDirection(Direction.DOWN);
        return frog;
    }


    public MonsterBulletInterface generateMonsterBullet(ATTACK type, Point start) {
        if (!ifMonsterBulletLoad) {
            this.mstAttackTextureAtlas.load();
            ifMonsterBulletLoad = true;
        }
        switch (type) {
            case FROMFROG:
                return new FrogBullet(start.getX(), start.getY(), this.mstAttackTextureRegion, this.getVertexBufferObjectManager());
        }
        return null;
    }


    public LightEffectAnm generateLightEffectAnm(float x, float y) {
        if (!ifLightEffectLoad) {
            this.mLightEffectTextureAtlas.load();
            ifLightEffectLoad = true;
        }
        return new LightEffectAnm(x, y, this.mLightEffectTextureRegion, this.getVertexBufferObjectManager());

    }

    public PlayerBulletInterface generatePlayerBullet(ATTACK type, Point start) {

        switch (type) {
            case FROM_PLAYER_A:
                return new PlayerABullet(start.getX(), start.getY(), this.mPlayerAttackBallRegion, this.getVertexBufferObjectManager());
            case FROM_PLAYER_B:

                return new PlayerBBullet(start.getX(), start.getY(), this.mPlayerAttackBallRegion, this.getVertexBufferObjectManager());
            case FROM_PLAYER_C:
                return new PlayerCBullet(start.getX(), start.getY(), this.mPlayerAttackBallRegion, this.getVertexBufferObjectManager());
            case FROM_PLAYER_D:
                return new PlayerDBullet(start.getX(), start.getY(), this.mPlayerAttackBallRegion, this.getVertexBufferObjectManager());
            default:
                return new PlayerABullet(start.getX(), start.getY(), this.mPlayerAttackBallRegion, this.getVertexBufferObjectManager());

        }
    }

    public NewPlayerItem generateNewPlayerItem(PlayerType type,Point pos){
        NewPlayerItem newPlayer;
        switch(type){
            case A:
                newPlayer= new NewPlayerItem(pos.getX(),pos.getY(),this.mPlayerATextureRegion,this.getVertexBufferObjectManager());
                newPlayer.setType(PlayerType.A);
                break;
            case B:
                newPlayer= new NewPlayerItem(pos.getX(),pos.getY(),this.mPlayerBTextureRegion,this.getVertexBufferObjectManager());
                newPlayer.setType(PlayerType.B);
                break;
            case C:
                newPlayer= new NewPlayerItem(pos.getX(),pos.getY(),this.mPlayerCTextureRegion,this.getVertexBufferObjectManager());
                newPlayer.setType(PlayerType.C);
                break;
            case D:
                newPlayer= new NewPlayerItem(pos.getX(),pos.getY(),this.mPlayerDTextureRegion,this.getVertexBufferObjectManager());
                newPlayer.setType(PlayerType.D);
                break;
            default: newPlayer= new NewPlayerItem(pos.getX(),pos.getY(),this.mPlayerATextureRegion,this.getVertexBufferObjectManager());
        }
        newPlayer.setCurrentTileIndex(7);

        return newPlayer;
    }


    @Override
    public void onBackPressed()
    {
        this.startActivity(new Intent(this, MenuActivity.class));
        finish();
    }
}




