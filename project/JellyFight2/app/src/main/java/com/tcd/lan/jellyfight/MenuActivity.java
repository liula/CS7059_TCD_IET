package com.tcd.lan.jellyfight;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.GLES20;

import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.tcd.lan.jellyfight.player.JellyFishPlayerA;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObject;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.VerticalAlign;

/**
 * Created by vlery on 2016/12/4.
 */

public class MenuActivity  extends SimpleBaseGameActivity {
    int deviceWidth, deviceHeight;
    private BitmapTextureAtlas mBitmapPlayerATextureAtlas;
    private TiledTextureRegion mPlayerATextureRegion;
    private BitmapTextureAtlas mBitmapPlayerBTextureAtlas;
    private TiledTextureRegion mPlayerBTextureRegion;
    private BitmapTextureAtlas mBitmapPlayerCTextureAtlas;
    private TiledTextureRegion mPlayerCTextureRegion;
    private BitmapTextureAtlas mBitmapPlayerDTextureAtlas;
    private TiledTextureRegion mPlayerDTextureRegion;
    private Scene scene;
    private BoundCamera camera;
    private Font font;
    private Font font_big;
    @Override
    protected void onCreateResources() {
        FontFactory.setAssetBasePath("font/");
        font = FontFactory.createFromAsset(this.getFontManager(), this.getTextureManager(), 512, 512, TextureOptions.BILINEAR, this.getAssets(), "Plok.ttf", 16, true, android.graphics.Color.BLACK);
        font.load();
        font_big = FontFactory.createFromAsset(this.getFontManager(), this.getTextureManager(), 512, 512, TextureOptions.BILINEAR, this.getAssets(), "Plok.ttf", 20, true, android.graphics.Color.BLACK);
        font_big.load();
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        this.mBitmapPlayerATextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerATextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerATextureAtlas, this, "JFplayer1.png", 0, 0, 3, 4);
        this.mBitmapPlayerATextureAtlas.load();

        this.mBitmapPlayerBTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerBTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerBTextureAtlas, this, "JFplayer2.png", 0, 0, 3, 4);
        this.mBitmapPlayerBTextureAtlas.load();

        this.mBitmapPlayerCTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerCTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerCTextureAtlas, this, "JFplayer3.png", 0, 0, 3, 4);
        this.mBitmapPlayerCTextureAtlas.load();

        this.mBitmapPlayerDTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 72, 128, TextureOptions.DEFAULT);
        this.mPlayerDTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapPlayerDTextureAtlas, this, "JFplayer4.png", 0, 0, 3, 4);
        this.mBitmapPlayerDTextureAtlas.load();
    }

    @Override
    protected Scene onCreateScene() {

        scene = new Scene();

      //  scene.setBackground(new Background(0.09804f, 0.6274f, 0.8784f));
        scene.setBackground(new Background(0.5f, 0.5f, 0.5f));
        Entity title =new Entity();
        Text mTitleText = new Text(0, 0,font , "groop fight", "groop fight".length(), this.getVertexBufferObjectManager());
        mTitleText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        mTitleText.setAlpha(1.0f);
        title .attachChild( mTitleText);
        title.setPosition((deviceWidth-mTitleText.getWidth())/2f,35);

        final Entity startButton = new Entity();

        final Text mStartText = new Text(0, 0,font , "start", "start".length(), this.getVertexBufferObjectManager());
        // mScoreText.setPosition((deviceWidth - mScoreText.getWidth()) , 10f);
        mStartText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        mStartText.setAlpha(1.0f);
        startButton .attachChild(mStartText);
        startButton.setPosition((float)deviceWidth/2f - mStartText.getWidth()/2f, 90);
        Line underStart = new Line(0,mStartText.getHeight(),mStartText.getWidth(),mStartText.getHeight(),this.getVertexBufferObjectManager());
        underStart.setLineWidth(5);

        underStart.setColor(0.0f,0.0f,0.0f);
        startButton.attachChild(underStart);




        final Entity exitButton = new Entity();

        final Text mExitText = new Text(0, 0,font , "exit", "exit".length(), this.getVertexBufferObjectManager());
        // mScoreText.setPosition((deviceWidth - mScoreText.getWidth()) , 10f);
        mExitText.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        mExitText.setAlpha(1.0f);
        exitButton .attachChild(mExitText);
        exitButton.setPosition((float)deviceWidth/2f - mExitText.getWidth()/2f, 130);
        Line underExit = new Line(0,mExitText.getHeight(),mExitText.getWidth(),mExitText.getHeight(),this.getVertexBufferObjectManager());
        underExit.setLineWidth(5);

        underExit.setColor(0.0f,0.0f,0.0f);
        exitButton.attachChild(underExit);


       AnimatedSprite a= new AnimatedSprite(0,0,this.mPlayerATextureRegion,this.getVertexBufferObjectManager());
        a.animate(new long[]{200,200,200}, 3, 5, true);
        AnimatedSprite b= new AnimatedSprite(a.getWidth(),0,this.mPlayerBTextureRegion,this.getVertexBufferObjectManager());
        b.animate(new long[]{200,200,200}, 3, 5, true);
        AnimatedSprite c= new AnimatedSprite(2*a.getWidth(),0,this.mPlayerCTextureRegion,this.getVertexBufferObjectManager());
        c.animate(new long[]{200,200,200}, 3, 5, true);
        AnimatedSprite d= new AnimatedSprite(3*a.getWidth(),0,this.mPlayerDTextureRegion,this.getVertexBufferObjectManager());
        d.animate(new long[]{200,200,200}, 3, 5, true);

        Entity role = new Entity();
        role.attachChild(a);
        role.attachChild(b);
        role.attachChild(c);
        role.attachChild(d);
        role.setPosition((deviceWidth-4*a.getWidth())/2,180);


        scene.registerTouchArea(mStartText);
        scene.registerTouchArea (mExitText);

        scene.setOnAreaTouchListener(
                new IOnAreaTouchListener() {
                    @Override
                    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final ITouchArea pTouchArea, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                    if(pSceneTouchEvent.isActionUp()) {

                        int pos_x= (int) (pSceneTouchEvent.getX() - startButton.getX() );
                        int pos_y = (int) (pSceneTouchEvent.getY() - startButton.getY());
                        if(pos_x<mStartText.getWidth()&& pos_y<mStartText.getHeight()){
                            jump();
                            return true;
                        }
                        pos_x=(int)(pSceneTouchEvent.getX() - exitButton.getX() );
                        pos_y = (int) (pSceneTouchEvent.getY() - exitButton.getY());
                        if(pos_x<mExitText.getWidth()&& pos_y<mExitText.getHeight()){
                            exit();
                        }


                    }
                    return true;
                    }
                });
        ;




        scene.attachChild(title);
        scene.attachChild(startButton);
        scene.attachChild(exitButton);
        scene.attachChild(role);
        return scene;
    }

    public void jump(){
        this.startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    public void exit(){
        System.exit(0);
    }
    @Override
    public EngineOptions onCreateEngineOptions() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        deviceWidth = displayMetrics.widthPixels / 4;
        deviceHeight = displayMetrics.heightPixels / 4;
        this.camera = new BoundCamera(0, 0, deviceWidth, deviceHeight);
        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(deviceWidth, deviceHeight), this.camera);

    }
}
