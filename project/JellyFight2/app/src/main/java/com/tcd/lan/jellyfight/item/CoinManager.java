package com.tcd.lan.jellyfight.item;

import com.tcd.lan.jellyfight.Game;
import com.tcd.lan.jellyfight.LightEffect;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.util.Config;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/15.
 */

public class CoinManager {

    private static int MAX_COUNT = 500;


    ArrayList<Coin> coinList;
    Game game;
    int generateCount ;

    public CoinManager(Game game){
        this.game = game;
        coinList = new ArrayList<Coin>();
        generateCount = 0;

    }



    public void generateRandom(){
        game.addCoin(0);
    }

    public void checkCollision(JellyFishPlayer leader){
        for(Coin coin:coinList){

            if(coin.collidesWith(leader)){
                coinList.remove(coin);
                coin.detachSelf();
                leader.addBlood(3);
                game.addScore(2);
                game.addLightEffect(LightEffect.GETCOIN,leader.getX()+ Config.PLAYER_X_OFFSET- Config.LE_ANM_WIDTH_OFFSET,leader.getY()+ Config.PLAYER_Y_OFFSET- Config.LE_ANM_HEIGHT_OFFSET);
                break;
            }
        }

    }

    public void handle(){
        generateCount++;
        generateCount%=MAX_COUNT;

        if(generateCount==0){
            generateRandom();
        }

        checkCollision(game.getLeader());
    }

    public void addCoin(Coin coin){
        coinList.add(coin);
    }


    public boolean checkLocVacancy(float pos_x,float pos_y){
        if(coinList.size()==0){
           return true;
        }
        for(Coin coin:coinList){
            if(pos_x<(coin.getX()+20)&&pos_x>(coin.getX()-20)&&pos_y<(coin.getY()+20)&&pos_y>(coin.getY()-20)){
                return false;
            }
        }
        return true;
    }
}
