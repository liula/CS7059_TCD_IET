package com.tcd.lan.jellyfight.item;

import com.tcd.lan.jellyfight.player.PlayerType;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/27.
 */

public class NewPlayerItem extends TiledSprite{
    static int Z_INDEX=2;
    PlayerType type;

    public NewPlayerItem(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setZIndex(2);
    }

    public void setType(PlayerType type){
        this.type = type;
    }
    public PlayerType getType(){
        return type;
    }
}
