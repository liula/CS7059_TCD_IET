package com.tcd.lan.jellyfight.monster;

import com.tcd.lan.jellyfight.ATTACK;
import com.tcd.lan.jellyfight.player.JFTroop;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/15.
 */

public class FrogBullet extends TiledSprite implements MonsterBulletInterface {
    static float HURT_RANGE= 50;
    static float HURT_DEGREE= 8;

    float v_x;
    float v_y;
    MonsterManager manager;
    static ATTACK type= ATTACK.FROMFROG;
    public FrogBullet(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
    }

    public void setVolecity(float x,float y){
        v_x=x;
        v_y=y;
    }
    @Override
    public void process(){
        setX(getX()+v_x);
        setY(getY()+v_y);
    }


    @Override
    public void setManager(MonsterManager manager) {
        this.manager = manager;
    }

    @Override
    public boolean checkCollisionWithTroop() {
        JFTroop troop = manager.getTroop();
        for(int i=0;i<troop.getMemberNum();i++) {
            JellyFishPlayer player = troop.getMember(i);
            if (this.collidesWith(player)) {
                explode(troop);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkActiveRequest() {
        return checkCollisionWithTroop();


    }
    private void explode(JFTroop troop){
        float center_x=getX()+ Config.FROG_BULLET_X_OFFSET;
        float center_y=getY()+ Config.FROG_BULLET_Y_OFFSET;
        Point center =new Point(center_x,center_y);
        for(int i=0;i<troop.getMemberNum();i++){
            JellyFishPlayer player =troop.getMember(i);
            float pos_x=player.getX()+ Config.PLAYER_X_OFFSET;
            float pos_y = player.getY()+ Config.PLAYER_Y_OFFSET;
            if(center.getDistance(new Point(pos_x,pos_y))<HURT_RANGE){
                player.getHurt(HURT_DEGREE);
            }


        }
       // manager.removeBullet(this);
        this.detachSelf();
        manager.addAttackActivateAnm(ATTACK.FROMFROG,center_x- Config.LE_ANM_WIDTH_OFFSET,center_y- Config.LE_ANM_HEIGHT_OFFSET);
    }


}
