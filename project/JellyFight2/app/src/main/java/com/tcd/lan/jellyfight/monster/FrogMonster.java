package com.tcd.lan.jellyfight.monster;

import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.player.JFTroop;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/14.
 */

public class FrogMonster extends Monster implements MonsterInterface{
    static float FULL_BLOOD =40;
    static float ATTACK_RANGE= 150;


    static float MAX_COUNT =150;

    public FrogMonster(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);

    }

    @Override
    public void attack() {
        if(ifAttack){
            attack_count++;
            attack_count%=MAX_COUNT;
            if(attack_count==0){
                Point p1= target;
                Point p2 = new Point(getX(),getY());
                if(Math.abs(p1.getX()-p2.getX())>Math.abs(p1.getY()-p2.getY())){
                    if(p1.getX()>p2.getX()){
                        setDirection(Direction.RIGHT);
                    }else if(p1.getX()<p2.getX()){
                        setDirection(Direction.LEFT);
                    }
                }else if(Math.abs(p1.getX()-p2.getX())<Math.abs(p1.getY()-p2.getY())){
                    if(p1.getY()>p2.getY()){
                        setDirection(Direction.DOWN);
                    }else if(p1.getY()<p2.getY()){
                        setDirection(Direction.UP);
                    }
                }
                manager.frogAttack(new Point(getX()+ Config.FROG_POS_X_OFFSET,getY()+Config.FROG_POS_Y_OFFSET),target);
            }

        }
    }


    @Override
    public void locate(JFTroop troop) {
        ifAttack = false;
        float temp_dis= 10000;
        for(int i=0;i<troop.getMemberNum();i++){
            JellyFishPlayer p= troop.getMember(i);
            Point p1=new Point(p.getX(),p.getY());
            Point p2=new Point (getX(),getY());
            float distance = p1.getDistance(p2);
            if(distance<ATTACK_RANGE&&distance<temp_dis){
                target = p1;
                temp_dis=distance;

                ifAttack= true;
            }
        }
    }

    @Override
    public boolean ifAlive() {
        return ifAlive;
    }

    @Override
    public void setFullBar() {

        bar.initBlood(FULL_BLOOD);

    }


}
