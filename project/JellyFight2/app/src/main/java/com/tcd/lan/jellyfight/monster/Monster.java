package com.tcd.lan.jellyfight.monster;

import com.tcd.lan.jellyfight.player.JFPlayerBloodBar;
import com.tcd.lan.jellyfight.player.JFTroop;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/14.
 */

public abstract class Monster  extends TiledSprite implements MonsterInterface{
    static float BAR_Y_OFFSET = -10;
    boolean ifAttack;
    Point target ;
    MonsterManager manager;
    int attack_count;
    boolean ifAlive;
    int full_blood;
    int current_blood;
    MonsterBloodBar bar ;
    public Monster(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        ifAttack= false;
        attack_count=0;
        ifAlive=true;
    }

    public void initBloodBar(MonsterBloodBar bar){
        this.bar = bar;
        setFullBar();
    }

    abstract public void attack() ;

    @Override
    public void setDirection(Direction direction) {

        switch (direction){
            case DOWN:
                this.setCurrentTileIndex(0);break;
            case UP:
                this.setCurrentTileIndex(3);break;
            case RIGHT:
                this.setCurrentTileIndex(2);break;
            case LEFT:
                this.setCurrentTileIndex(1);break;
        }

    }

    @Override
    abstract  public void locate(JFTroop troop) ;

    @Override
    public void setManager(MonsterManager manager) {
        this.manager= manager;
    }

    @Override
    public void getHurt(float degree) {
       if(! bar.decrease(degree)){
         die();
       }

    }

    public void die(){
        manager.proecessMonsterDeath(this);

        ifAlive=false;
        detachSelf();
        bar.detachSelf();

    }
    public abstract void setFullBar();


    public float getBarYOffset(){
        return BAR_Y_OFFSET;
    }

    public MonsterBloodBar getBloodBar(){
        return bar;
    }
}
