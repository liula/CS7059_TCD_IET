package com.tcd.lan.jellyfight.monster;

import com.tcd.lan.jellyfight.BulletInterface;
import com.tcd.lan.jellyfight.player.JFTroop;

/**
 * Created by vlery on 2016/11/18.
 */

public interface MonsterBulletInterface extends BulletInterface {

    public void setManager(MonsterManager manager);
    public boolean checkCollisionWithTroop();

}
