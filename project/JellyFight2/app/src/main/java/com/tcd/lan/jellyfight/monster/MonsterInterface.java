package com.tcd.lan.jellyfight.monster;

import android.service.quicksettings.Tile;

import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.player.JFTroop;

import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.TiledSprite;

/**
 * Created by vlery on 2016/11/14.
 */

public interface MonsterInterface extends IShape {

    public void attack();

    public void setDirection(Direction direction);

    public void locate(JFTroop troop);

    public void setManager(MonsterManager manager);

    public void getHurt(float degree);
    public boolean ifAlive();
}
