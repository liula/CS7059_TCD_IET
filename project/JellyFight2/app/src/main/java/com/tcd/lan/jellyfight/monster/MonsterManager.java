package com.tcd.lan.jellyfight.monster;

import com.tcd.lan.jellyfight.ATTACK;
import com.tcd.lan.jellyfight.Border;
import com.tcd.lan.jellyfight.Borderline;
import com.tcd.lan.jellyfight.BulletInterface;
import com.tcd.lan.jellyfight.Game;
import com.tcd.lan.jellyfight.JFSpriteAnmInterface;
import com.tcd.lan.jellyfight.LightEffect;
import com.tcd.lan.jellyfight.player.JFTroop;
import com.tcd.lan.jellyfight.player.JellyFishPlayer;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Point;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/15.
 */


public class MonsterManager {
    static int MAX_COUNT = 300;

    ArrayList<MonsterInterface>  monsterList;
    ArrayList<MonsterBulletInterface> bulletList;
    int generateCount;
    Game game;

    public MonsterManager(Game game){
        monsterList  = new ArrayList<MonsterInterface>();
        this.game= game;
        generateCount =0;
        bulletList =new ArrayList<MonsterBulletInterface>();
    }
    public void addMonster(MonsterInterface monster){
        monsterList.add(monster);
        monster.setManager(this);
    }
    public void addBullet(MonsterBulletInterface bullet){
        bulletList.add(bullet);
        bullet.setManager(this);
    }

    public int getMonsterNum(){
        return monsterList.size();
    }
    public MonsterInterface getMonsterByIndex(int i){
        return monsterList.get(i);
    }

    public void handle(){
        generateMonster();
        removeDiedMonster();
        attack();
        ArrayList<MonsterBulletInterface> needMove = new ArrayList<MonsterBulletInterface>();
        for(MonsterBulletInterface bullet:bulletList){
            bullet.process();
            if(bullet.checkActiveRequest()){
                needMove.add(bullet);
            }
        }
        for(MonsterBulletInterface bullet : needMove){
            bulletList.remove(bullet);
        }
        processWithBorder();


    }

    private void  removeDiedMonster(){
        ArrayList<MonsterInterface> needMove = new ArrayList<MonsterInterface>();
        for(MonsterInterface monster:monsterList){
                if(!monster.ifAlive()){
                    needMove.add(monster);
                }
        }
        for(MonsterInterface monster:needMove) {
            monsterList.remove(monster);
        }
    }

    void generateMonster(){
        generateCount++;
        generateCount%=MAX_COUNT;

        if(generateCount==0){
            generateMonsterByType(0);
        }
    }

    void generateMonsterByType(int type){
        switch(type){
            case 0:
                game.addFrogMonster();
                break;
        }
    }
    public void attack(){
            for(MonsterInterface monster:monsterList){
                monster.locate(game.getTroop());
                monster.attack();
            }
    }


    public void frogAttack(Point start, Point target){
            game.addMonsterBullet(ATTACK.FROMFROG ,start,target);
    }
    public void processWithBorder(){
        Border border = game.getBorder();
        for(int i=0;i<border.getLineNum();i++){
            Borderline line = border.getLine(i);
            ArrayList<MonsterBulletInterface> needRemove= new ArrayList<MonsterBulletInterface>();
            for(MonsterBulletInterface bullet:bulletList){
                if(line.collidesWith(bullet)){
                    bullet.detachSelf();
                    needRemove .add(bullet);
                }
            }
            for(MonsterBulletInterface bullet:needRemove){
                    bulletList.remove(bullet);
            }
        }
    }



    public void addAttackActivateAnm(ATTACK type,float x,float y){
        switch(type){
            case FROMFROG: game.addLightEffect(LightEffect.FROGBULLETATTACK,x,y);break;
        }
    }


    public void finishBullet(){
        for(MonsterBulletInterface bullet:bulletList){
            bullet.detachSelf();

        }
        bulletList.clear();
    }

    public JFTroop getTroop(){
        return game.getTroop();
    }


    public void proecessMonsterDeath(Monster monster){
        float x=monster.getX()+ Config.FROG_POS_X_OFFSET;
        float y=monster.getY()+Config.FROG_POS_Y_OFFSET;
        game.addScore(1);
        game.addMonsterDeathReward(x,y);
        game.addMonsterDieAnm(x,y);

    }

    public boolean checkCollision(JellyFishPlayer leader) {

        for(MonsterInterface monster : monsterList){
            if(monster.collidesWith(leader)){
                return true;
            }
        }
        return false;
    }
}
