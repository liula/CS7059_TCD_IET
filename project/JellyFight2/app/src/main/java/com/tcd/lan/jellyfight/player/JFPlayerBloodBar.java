package com.tcd.lan.jellyfight.player;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by vlery on 2016/11/14.
 */

public class JFPlayerBloodBar extends TiledSprite{
    static int Z_INDEX=3;
    float fullBlood ;
    float crtBlood;
    boolean ifAlive ;
    private int tileIndex ;
    public JFPlayerBloodBar(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setCurrentTileIndex(0);
        this.setZIndex(Z_INDEX);
    }

    public void initBlood(float fullBlood){
        this.fullBlood=fullBlood;
        crtBlood= fullBlood;
        this.setCurrentTileIndex(0);
        tileIndex=0;
        ifAlive =true;
    }

    public boolean decrease(float lose){

        crtBlood-=lose;
        if(crtBlood>0){

           updateTile();
        }else{
            ifAlive= false;
        }
        return ifAlive;
    }

    private void updateTile(){
        float rate = crtBlood/fullBlood;
        if(rate ==1){
            if(tileIndex!=0) {
                setCurrentTileIndex(0);
                tileIndex=0;
            }
        }else  if(rate>0.9){
            if(tileIndex!=1) {
                setCurrentTileIndex(1);
                tileIndex=1;
            }
        }else if(rate >0.8){
            if(tileIndex!=2) {
                setCurrentTileIndex(2);
                tileIndex=2;
            }
        }else if(rate >0.7){
            if(tileIndex!=3) {
                setCurrentTileIndex(3);
                tileIndex=3;
            }
        }else if(rate >0.6){
            if(tileIndex!=4) {
                setCurrentTileIndex(4);
                tileIndex=4;
            }
        }else if(rate >0.5){
            if(tileIndex!=5) {
                setCurrentTileIndex(5);
                tileIndex=5;
            }
        }else if(rate >0.4){
            if(tileIndex!=6) {
                setCurrentTileIndex(6);
                tileIndex=6;
            }
        }else if(rate >0.3){
            if(tileIndex!=7) {
                setCurrentTileIndex(7);
                tileIndex=7;
            }
        }else if(rate >0.2){
            if(tileIndex!=8) {
                setCurrentTileIndex(8);
                tileIndex=8;
            }
        }else if (rate >0.1){
            if(tileIndex!=9) {
                setCurrentTileIndex(9);
                tileIndex=9;
            }
        }else {
            if(tileIndex!=10) {
                setCurrentTileIndex(10);
                tileIndex=10;
            }
        }
    }

    public void increase(float addBlood) {
        crtBlood+=addBlood;
        if(crtBlood>fullBlood) {
            crtBlood = fullBlood;
        }
            updateTile();

    }
}
