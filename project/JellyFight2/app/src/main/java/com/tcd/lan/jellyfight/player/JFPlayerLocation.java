package com.tcd.lan.jellyfight.player;

/**
 * Created by vlery on 2016/11/9.
 */

public class JFPlayerLocation {

    float x;
    float y;

    public JFPlayerLocation(float x, float y){
        this.x=x;
        this.y= y;
    }
    public void setX(float x){
        this.x= x;

    }
    public void setY(float y){
        this.y= y;
    }
    public float getX(){
        return x;
    }
    public float getY(){
        return y;
    }

    public float getDistance(JFPlayerLocation location){
        float dis_x=location.getX();
        float dis_y=location.getY();

        double dis_pow= Math.pow((double)(dis_x-x),2)+Math.pow((double)(dis_y-y),2);
        return (float)Math.sqrt(dis_pow);
    }
}
