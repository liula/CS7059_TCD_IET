package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.util.Direction;

/**
 * Created by vlery on 2016/11/9.
 */

public class JFPlayerTurnInstruction {

    Direction direction;
    JFPlayerLocation location;

    public JFPlayerTurnInstruction(Direction direction,int x,int y){
        this.direction= direction;
        location=new JFPlayerLocation(x,y);
    }

    public boolean check(int x,int y){
        if(location.x==x&&location.y==y){
            return true;
        }
        else{
            return false;
        }
    }

    public Direction getDirection(){
        return direction;
    }
}
