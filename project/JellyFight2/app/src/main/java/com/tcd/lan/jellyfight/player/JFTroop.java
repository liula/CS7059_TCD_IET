package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.ATTACK;
import com.tcd.lan.jellyfight.Border;
import com.tcd.lan.jellyfight.Borderline;
import com.tcd.lan.jellyfight.Game;
import com.tcd.lan.jellyfight.LightEffect;
import com.tcd.lan.jellyfight.item.NewPlayerItem;
import com.tcd.lan.jellyfight.monster.MonsterBulletInterface;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/9.
 */

public class JFTroop {

    ArrayList<JellyFishPlayer> troop;
    JellyFishPlayer leader;
    ArrayList<PlayerType> unused;
    Game game;

    ArrayList<NewPlayerItem>  newPlayerList;

    ArrayList<PlayerBulletInterface> bulletList;
    public JFTroop(Game game){
        troop =new ArrayList<JellyFishPlayer>();
        unused= new ArrayList<PlayerType>();
        newPlayerList = new ArrayList<NewPlayerItem>();
        unused.add(PlayerType.A);
        unused.add(PlayerType.B);
        unused.add(PlayerType.C);
        unused.add(PlayerType.D);
        bulletList = new ArrayList<PlayerBulletInterface>();
        this.game = game;

    }

    public boolean ifAllTypeUse(){
        if(unused.size()==0)
            return true;
        else
            return false;

    }
    public PlayerType getRandomUnusedType(){
        int size= unused.size();

        int randomIndex= (int)Math.floor(Math.random()*size);

        PlayerType type;

        switch(unused.get(randomIndex)){
            case A:
                type=PlayerType.A;break;
            case B:
                type = PlayerType.B;break;
            case C:
                type = PlayerType.C;break;
            case D:
                type = PlayerType.D;break;
            default:type =PlayerType.A;
        }

        unused.remove(randomIndex);

        return type;
    }
    public PlayerType getType(){
        int size= unused.size();

        int randomIndex= 0;

        PlayerType type;

        switch(unused.get(randomIndex)){
            case A:
                type=PlayerType.A;break;
            case B:
                type = PlayerType.B;break;
            case C:
                type = PlayerType.C;break;
            case D:
                type = PlayerType.D;break;
            default:type =PlayerType.A;
        }

        unused.remove(randomIndex);

        return type;

    }
    public void addMember(JellyFishPlayer player){
       player.setTroop(this);
        if(troop.size()==0) {
            troop.add(player);
            leader= player;
            leader.setTroopIndex(0);
        }else {

            JellyFishPlayer former = troop.get(troop.size()-1);
            player.setTurnList(former.getTurnList());
            //System.out.println("!!!!!!!"+former.getTurnList().size());
            player.setLocation(new JFPlayerLocation(former.getX(), former.getY()), former.getDirection());
            player.setDirection(former.getDirection());
            player .setTroopIndex(troop.size());
            troop.add(player);
        }
    }

    public boolean collideWithSelf(){
        for(int i=1;i<troop.size();i++){
            if(leader.collidesWith(troop.get(i))){
                return true;
            }
        }
        return false;
    }
    public void removeAll(){
        troop.clear();
    }
    public int getMemberNum(){
        return troop.size();
    }
    public JellyFishPlayer getMember(int i){
        return troop.get(i);
    }

    public JellyFishPlayer getLeader(){
        return leader;
    }

    public void step(){
        for (JellyFishPlayer player:troop){
            player.step();

            player.locate(game.getMonsterManager());
            player.attack();

        }
        broadcastDeath();
        ArrayList<PlayerBulletInterface> needMove = new ArrayList<PlayerBulletInterface>();
        for(PlayerBulletInterface bullet:bulletList){
            bullet.process();
            if(bullet.checkActiveRequest()){
                needMove.add(bullet);
            }
        }
        for(PlayerBulletInterface bullet : needMove){
            bulletList.remove(bullet);
        }

        for(NewPlayerItem newplayer :newPlayerList){
            if(newplayer.collidesWith(leader)){
                game.addPlayer(newplayer.getType());
                game.addScore(1);
                newPlayerList.remove(newplayer);
                newplayer.detachSelf();
                break;
            }
        }
//        if(collideWithSelf()){
//            game.end();
//        }
        processWithBorder();
    }

    public void broadcastDeath(){
        ArrayList<JellyFishPlayer> needmove = new ArrayList<JellyFishPlayer>();
        for(JellyFishPlayer player :troop) {
            int index = player.getTroopIndex();
            if (player.getIfBroadcasetDeath()) {
                if(index==0){
                    game.end();
                    break;
                }
                for (int i = index; i < troop.size()-1 ; i++) {
                    JellyFishPlayer player1 = troop.get(i);
                    JellyFishPlayer player2 = troop.get(i + 1);
                    player2.setX(player1.getX());
                    player2.setY(player1.getY());
                    player2.setDirection(player1.getDirection());
                    player2.clearTurnList();
                    player2.setTurnList(player1.getTurnList());
                    player2.setTroopIndex(player1.getTroopIndex());
                    player2.bar.setX(player1.bar.getX());
                    player2.bar.setY(player1.bar.getY());
                }
                needmove .add(player);
            }

        }

        for(JellyFishPlayer p :needmove){
            unused.add(p.getType());
            troop.remove(p);
        }
    }

    public void addBullet(PlayerBulletInterface bullet){
        bulletList.add(bullet);
        bullet.setTroop(this);
    }


    public Direction getLeaderDirection(){
        return leader.getDirection();
    }

    public void turn(Direction direction){
        JFPlayerTurnInstruction turn;
        leader.setDirection(direction);
        turn = new JFPlayerTurnInstruction(direction,(int)leader.getX(),(int)leader.getY());
        for(int i=1;i<troop.size();i++){
            troop.get(i).addTurnInstruction(turn);
        }

    }


    public void addNewPlayerItem(NewPlayerItem playerItem){
        newPlayerList.add(playerItem);
    }

    public boolean checkLocVacancy(float pos_x,float pos_y){
        for(JellyFishPlayer player:troop){
            if(pos_x<(player.getX()+20)&&pos_x>(player.getX()-20)&&pos_y<(player.getY()+20)&&pos_y>(player.getY()-20)){
                return false;
            }
        }
        return true;
    }


    public void playerAAttack(Point start,Point target){
        game.addPlayerBullet(ATTACK.FROM_PLAYER_A ,start,target);
    }

    public void playerBAttack(Point start,Point target){
        game.addPlayerBullet(ATTACK.FROM_PLAYER_B ,start,target);
    }

    public void  playerCAttack(Point start,Point target){
        game.addPlayerBullet(ATTACK.FROM_PLAYER_C ,start,target);
    }
    public void playerDAttack(Point start,Point target){
        game.addPlayerBullet(ATTACK.FROM_PLAYER_D ,start,target);
    }
    public void processWithBorder(){
        Border border = game.getBorder();
        for(int i=0;i<border.getLineNum();i++){
            Borderline line = border.getLine(i);
            ArrayList<PlayerBulletInterface> needRemove= new ArrayList<PlayerBulletInterface>();
            for(PlayerBulletInterface bullet:bulletList){
                if(line.collidesWith(bullet)){
                    bullet.detachSelf();
                    needRemove.add(bullet);
                }
            }
            for(PlayerBulletInterface bullet:needRemove){
                bulletList.remove(bullet);
            }
        }
    }


    public MonsterManager getMonsterManager(){
        return game.getMonsterManager();

    }


    public void addAttackActivateAnm(ATTACK type,float x,float y){

        switch(type){
            case FROM_PLAYER_A:
                game.addLightEffect(LightEffect.PLAYER_A_ATTACK,x,y);
                break;
            case FROM_PLAYER_B:
                game.addLightEffect(LightEffect.PLAYER_B_ATTACK,x,y);
                break;
            case FROM_PLAYER_C:
                game.addLightEffect(LightEffect.PLAYER_C_ATTACK,x-20,y+20);
                game.addLightEffect(LightEffect.PLAYER_C_ATTACK,x+20,y+20);
                game.addLightEffect(LightEffect.PLAYER_C_ATTACK,x-20,y-20);
                game.addLightEffect(LightEffect.PLAYER_C_ATTACK,x+20,y-20);

                break;
            case FROM_PLAYER_D:
                game.addLightEffect(LightEffect.PLAYER_D_ATTACK,x,y);
                break;
        }

    }


    public void processPlayerDeath(JellyFishPlayer player){



            player.setIfBroadcastDeath(true);
            killPlayer(player);

//            JellyFishPlayer next_player = troop.get(index + 1);
//            next_player.setIfBroadcastDeath(true);
//            if(player.getIfBroadcasetDeath()){
//                next_player.setPhantom(player.getPhantom());
//            }else {
//                JellyFishPlayer phamton = game.getNewPhamton();
//                phamton.setDirection(player.getDirection());
//                phamton.setTurnList(player.getTurnList());
//                next_player.setPhantom(phamton);
//            }


    }

    public void killPlayer(JellyFishPlayer player){
        game.addPlayerDeathAnm(player.getX()+ Config.PLAYER_X_OFFSET,player.getY()+Config.PLAYER_Y_OFFSET);
        player.detachSelf();
        player.getBloodBar().detachSelf();
    }


    public void finishBullet(){
        for(PlayerBulletInterface bullet:bulletList){
            bullet.detachSelf();
        }
        bulletList.clear();
    }
    public void killTroop(JFTroop troop){
       //ArrayList<JellyFishPlayer> needMove = new ArrayList<JellyFishPlayer>();
        for(int i=0;i<troop.getMemberNum();i++){
            JellyFishPlayer player =troop.getMember(i);
            killPlayer(player);
        }
        troop.removeAll();
    }
}
