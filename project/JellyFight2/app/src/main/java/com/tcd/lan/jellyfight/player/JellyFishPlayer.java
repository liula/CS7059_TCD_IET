package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.monster.Monster;
import com.tcd.lan.jellyfight.monster.MonsterInterface;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/9.
 */

public abstract class JellyFishPlayer extends AnimatedSprite {
    static float BAR_Y_OFFSET = -10;
    Direction direction;
    int troopIndex;
    private static final float TROOP_Distance =32;
    static int VOLECITY =2;
    static int Z_INDEX=2;
    int attack_count;
    JFPlayerBloodBar bar ;
    JFTroop troop ;
    boolean ifAttack;
    Point target;
    boolean ifBroadcastDeath;
    JellyFishPlayer phantom;
    PlayerType type;
    ArrayList<JFPlayerTurnInstruction> turnList;




    public JellyFishPlayer(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        turnList =new ArrayList<JFPlayerTurnInstruction>();
        this.setZIndex(Z_INDEX);
        ifBroadcastDeath= false;
    }

    public void setTroopIndex(int index){
        troopIndex = index;
    }

    public int getTroopIndex (){
        return troopIndex;
    }

    public void setTurnList (ArrayList<JFPlayerTurnInstruction> turnList){

        for(int i=0;i<turnList.size();i++){
            this.turnList.add(turnList.get(i));
        }

    }
    public void clearTurnList(){
        turnList.clear();
    }
    public ArrayList<JFPlayerTurnInstruction>  getTurnList(){
        return turnList;
    }
    public void setTroop(JFTroop troop){
        this.troop = troop;
    }
    public float getBarYOffset(){
        return BAR_Y_OFFSET;
    }
    public void setLocation(JFPlayerLocation formerLocation,Direction formerDirection){
        switch(formerDirection){
            case UP:
                this.setX(formerLocation.x);
                this.setY(formerLocation.y+TROOP_Distance);
                break;
            case DOWN:
                this.setX(formerLocation.x);
                this.setY(formerLocation.y-TROOP_Distance);
                break;
            case RIGHT:
                this.setX(formerLocation.x-TROOP_Distance);
                this.setY(formerLocation.y);
                break;
            case LEFT:
                this.setX(formerLocation.x+TROOP_Distance);
                this.setY(formerLocation.y);
                break;
        }
    //    System.out.println(this.getX()+";;;"+this.getY());

    }

    public void setDirection(Direction direction){
        this.direction = direction;
        updateAnimatedPicture();
    }

    private void updateAnimatedPicture(){
        switch(direction){
            case DOWN:
                this.animate(new long[]{200, 200, 200}, 6, 8, true);
                break;
            case UP:
                this.animate(new long[]{200,200,200}, 0, 2, true);
                break;
            case RIGHT:
                this.animate(new long[]{200,200,200}, 3, 5, true);
                break;
            case LEFT:
                this.animate(new long[]{200,200,200}, 9, 11, true);
                break;
        }
    }
    public Direction getDirection(){
        return direction;
    }

    public void addTurnInstruction(JFPlayerTurnInstruction ti){
        turnList.add(ti);
    }
    public void step(){
        float x= this.getX();
        float y= this.getY();
        switch(direction){
            case UP:
                y-=VOLECITY;break;
            case DOWN:
                y+=VOLECITY;break;
            case RIGHT:
                x+=VOLECITY;break;
            case LEFT:
                x-=VOLECITY;break;
        }
        this.setX(x);
        this.setY(y);
        if(turnList.size()>0) {
            if (turnList.get(0).check((int) x, (int) y)) {
                    setDirection(turnList.get(0).direction);
                    turnList.remove(0);
            }
        }
        bar.setX(x);
        bar.setY(y+BAR_Y_OFFSET);

       // getHurt((float) 0.01);
    }


    public void initBloodBar(JFPlayerBloodBar bar){
        this.bar = bar;
        setFullBar();
    }
    public JFPlayerBloodBar generateBloodBar(ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager){
            bar= new JFPlayerBloodBar(this.getX(),this.getY()+BAR_Y_OFFSET,pTiledTextureRegion,pVertexBufferObjectManager);
            return bar;
    }

    public JFPlayerBloodBar getBloodBar(){
        return bar;
    }

    public void getHurt(float loseBlood){
        if(!bar.decrease(loseBlood)) {
            troop.processPlayerDeath(this);
        }
    }


    public void addBlood(float addBlood){
        bar.increase(addBlood);
    }
    public abstract void locate(MonsterManager monsterManager);
    public abstract void attack();

    public abstract void setFullBar();


   public void setIfBroadcastDeath(boolean ifBroadcastDeath){
       this.ifBroadcastDeath = ifBroadcastDeath;
   }

    public boolean getIfBroadcasetDeath(){
        return ifBroadcastDeath;
    }
    public void setPhantom(JellyFishPlayer player){
        phantom =player;
    }

    public JellyFishPlayer getPhantom(){
        return phantom;
    }

    public void setType(PlayerType type){
        this.type= type;
    }
    public PlayerType getType(){
        return type;
    }
}
