package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.monster.MonsterInterface;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/18.
 */

public class JellyFishPlayerA extends JellyFishPlayer {
   static float FULL_BLOOD=100;

    static float PLAYER_A_ATTACK_RANGE = 130;

    public JellyFishPlayerA(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        attack_count =0;
        ifAttack =false;
    }
    @Override
    public void locate(MonsterManager monsterManager){
        ifAttack= false;
        float temp_dis= 10000;
        for(int i=0;i<monsterManager.getMonsterNum();i++){
            MonsterInterface m= monsterManager.getMonsterByIndex(i);

            Point p1=new Point(m.getX(),m.getY());
            Point p2=new Point (getX(),getY());
            float distance = p1.getDistance(p2);
            if(distance<PLAYER_A_ATTACK_RANGE&&distance<temp_dis){
                target = p1;
                temp_dis=distance;
                ifAttack= true;
            }
        }


    }
    public void attack(){

        if(ifAttack) {
            attack_count++;
            attack_count %= Config.PLAYER_A_ATTACK_PERIOD;
            if (attack_count == 0) {

                troop.playerAAttack(new Point(getX()+Config.PLAYER_X_OFFSET,getY()+Config.PLAYER_Y_OFFSET),target);
            }
        }

    }


    @Override
    public void setFullBar() {
        bar.initBlood(FULL_BLOOD);
    }
}
