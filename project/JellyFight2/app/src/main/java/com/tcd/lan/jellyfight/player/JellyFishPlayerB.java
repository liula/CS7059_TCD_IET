package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.monster.MonsterInterface;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Direction;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.ArrayList;

/**
 * Created by vlery on 2016/11/18.
 */

public class JellyFishPlayerB extends JellyFishPlayer {
    static float FULL_BLOOD=60;

    public JellyFishPlayerB(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        attack_count =0;
        ifAttack =false;
    }

    @Override
    public void attack() {

        if(ifAttack) {
            attack_count++;
            attack_count %= Config.PLAYER_B_ATTACK_PERIOD;
            if (attack_count == 0) {

                troop.playerBAttack(new Point(getX()+Config.PLAYER_X_OFFSET,getY()+Config.PLAYER_Y_OFFSET),target);
            }
        }


    }
    @Override
    public void locate(MonsterManager monsterManager){
        ifAttack= false;
        //float temp_dis= 10000;
        for(int i=0;i<monsterManager.getMonsterNum();i++){
            MonsterInterface m= monsterManager.getMonsterByIndex(i);

            Point p1=new Point(m.getX(),m.getY());
            if(p1.getY()<getY()+15&& p1.getY()>getY()-15){
                if(direction== Direction.LEFT&&p1.getX()<getX()) {
                    target = p1;
                    ifAttack = true;
                }else if(direction== Direction.RIGHT&& p1.getX()>getX()){
                    target = p1;
                    ifAttack = true;
                }
            }else if(p1.getX()<getX()+15&&p1.getX()>getX()-15){
                if(direction== Direction.DOWN&&p1.getY()>getY()) {
                    target = p1;
                    ifAttack = true;
                }else if(direction== Direction.UP&& p1.getY()<getY()){
                    target = p1;
                    ifAttack = true;
                }
            }
        }


    }
    @Override
    public void setFullBar() {
        bar.initBlood(FULL_BLOOD);
    }
}
