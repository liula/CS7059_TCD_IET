package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.ATTACK;
import com.tcd.lan.jellyfight.monster.MonsterInterface;
import com.tcd.lan.jellyfight.monster.MonsterManager;
import com.tcd.lan.jellyfight.util.Config;
import com.tcd.lan.jellyfight.util.Point;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.IEntityComparator;
import org.andengine.entity.IEntityMatcher;
import org.andengine.entity.IEntityParameterCallable;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.IVertexBufferObject;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.IDisposable;
import org.andengine.util.adt.transformation.Transformation;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlery on 2016/11/20.
 */

public class PlayerABullet extends TiledSprite implements  PlayerBulletInterface {
    JFTroop troop;
    float v_x;
    float v_y;

    static float HURT_RANGE = 50;

    static float HURT_DEGREE = 10;
    public PlayerABullet(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        this.setCurrentTileIndex(Config.PLAYER_BULLET_FROM_A_INDEX);
    }
    @Override
    public void setVolecity(float x,float y){
        v_x=x;
        v_y=y;
    }


    @Override
    public void process() {
        setX(getX()+v_x);
        setY(getY()+v_y);
    }



    @Override
    public boolean checkActiveRequest() {
        return checkCollisionWithMonsters(troop.getMonsterManager());

    }

    private boolean checkCollisionWithMonsters(MonsterManager monsterManager){
        for(int i=0;i<monsterManager.getMonsterNum();i++){
            MonsterInterface m = monsterManager.getMonsterByIndex(i);
            if (this.collidesWith(m)) {
                explode(monsterManager);
                return true;
            }
        }
        return false;
    }
    @Override
    public void setTroop(JFTroop troop) {
        this.troop= troop;
    }





    private void explode(MonsterManager monsterManager){
        float center_x=getX()+ Config.PLAYER_X_OFFSET;
        float center_y=getY()+ Config.PLAYER_Y_OFFSET;
        Point center =new Point(center_x,center_y);


        for(int i=0;i<monsterManager.getMonsterNum();i++){
            MonsterInterface m= monsterManager.getMonsterByIndex(i);
            float pos_x=m.getX()+ Config.PLAYER_BULLET_X_OFFSET;
            float pos_y = m.getY()+ Config.PLAYER_BULLET_Y_OFFSET;
            if(center.getDistance(new Point(pos_x,pos_y))<HURT_RANGE){
               // player.getHurt(HURT_DEGREE);
                m.getHurt(HURT_DEGREE);
            }
        }
        // manager.removeBullet(this);
        this.detachSelf();
        troop.addAttackActivateAnm(ATTACK.FROM_PLAYER_A,center_x- Config.LE_ANM_WIDTH_OFFSET,center_y- Config.LE_ANM_HEIGHT_OFFSET);
    }

}
