package com.tcd.lan.jellyfight.player;

import com.tcd.lan.jellyfight.BulletInterface;

/**
 * Created by vlery on 2016/11/20.
 */

public interface PlayerBulletInterface extends BulletInterface {

    public void setTroop(JFTroop troop);
    public boolean checkActiveRequest();
    public void setVolecity(float x,float y);
}
