package com.tcd.lan.jellyfight.util;

/**
 * Created by vlery on 2016/11/18.
 */

public class Config {


    public static float LE_ANM_WIDTH_OFFSET=25;
    public static float LE_ANM_HEIGHT_OFFSET =25;
    public static float PLAYER_X_OFFSET=12;
    public static float PLAYER_Y_OFFSET= 16;
    public static float FROG_POS_X_OFFSET=12;
    public static float FROG_POS_Y_OFFSET= 9;
    public static float COIN_POS_X_OFFSET=5;
    public static float COIN_POS_Y_OFFSET= 6;


    public static float FROG_BULLET_X_OFFSET=5;
    public static float FROG_BULLET_Y_OFFSET=5;
    public static int PLAYER_BULLET_X_OFFSET =5;
    public static int PLAYER_BULLET_Y_OFFSET =5;
    public static int PLAYER_BULLET_FROM_A_INDEX =0;
    public static int PLAYER_BULLET_FROM_B_INDEX =3;
    public static int PLAYER_BULLET_FROM_C_INDEX =2;
    public static int PLAYER_BULLET_FROM_D_INDEX =1;

    public static int PLAYER_A_ATTACK_PERIOD=50;
    public static int PLAYER_B_ATTACK_PERIOD=80;
    public static int PLAYER_C_ATTACK_PERIOD=63;
    public static int PLAYER_D_ATTACK_PERIOD=95;




}
