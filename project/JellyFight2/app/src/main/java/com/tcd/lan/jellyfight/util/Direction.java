package com.tcd.lan.jellyfight.util;

/**
 * Created by vlery on 2016/11/9.
 */

public enum Direction {
    UP,DOWN,LEFT,RIGHT
}
