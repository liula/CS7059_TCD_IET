package com.tcd.lan.jellyfight.util;

/**
 * Created by vlery on 2016/11/15.
 */

public class Point {
    float x,y;
    public Point(float x, float y){
        this.x=x;
        this.y =y;
    }

    public float getX(){
        return x;
    }
    public float getY(){
        return y;
    }

    public float getDistance(Point p){
        float dis_x = p.getX()-x;
        float dis_y = p.getY()-y;
        double value =Math.pow(dis_x,2)+Math.pow(dis_y,2);
        value =Math.sqrt(value);
        return (float)value;
    }

    public Point normalize(){

        Point p = new Point(x,y);
        float dis= p.getDistance(new Point(0,0));
        p.x=p.x/dis;
        p.y=p.y/dis;

        return p;
    }
}
