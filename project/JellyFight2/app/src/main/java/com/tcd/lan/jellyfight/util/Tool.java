package com.tcd.lan.jellyfight.util;

import java.util.Vector;

/**
 * Created by vlery on 2016/11/18.
 */

public class Tool {

   public  static float getRotate(Point pos, Point target){

       float degree=(float)(Math.toDegrees(Math.acos((target.x-pos.x)/pos.getDistance(target))));
       if(target.y-pos.y>0) {
           return -degree;
       }else{
           return degree;
       }
   }
 }
